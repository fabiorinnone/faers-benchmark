package eu.fabiorinnone.faersbenchmark.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by fabior on 09/03/16.
 */
public class MySQLAdapter {

    private static MySQLAdapter instance = null;

    private String dbName;
    private Connection con;
    private String dbms;

    private MySQLAdapter(Connection connArg, String dbNameArg, String dbmsArg) {
        super();
        this.con = connArg;
        this.dbName = dbNameArg;
        this.dbms = dbmsArg;
    }

    public static synchronized MySQLAdapter getInstance(Connection connArg,
                                                             String dbNameArg,
                                                             String dbmsArg) {
        if (instance == null) {
            instance = new MySQLAdapter(connArg, dbNameArg, dbmsArg);
        }
        return instance;
    }

    public int executeQuery(String query) throws SQLException {
        Statement stmt = null;
        int count = -1;
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            rs.last();
            count = rs.getRow();
        } catch (SQLException e) {
            JDBCUtilities.printSQLException(e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

        return count;
    }
}
