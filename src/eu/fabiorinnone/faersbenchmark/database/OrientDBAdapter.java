package eu.fabiorinnone.faersbenchmark.database;

import com.google.common.collect.Iterables;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.tinkerpop.blueprints.impls.orient.OrientBaseGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphNoTx;
import sun.security.provider.certpath.Vertex;

/**
 * Created by fabior on 09/03/16.
 */
public class OrientDBAdapter {

    private static OrientDBAdapter instance =  null;

    private OrientBaseGraph graph;

    private OrientDBAdapter(OrientBaseGraph graph) {
        this.graph = graph;
    }

    public static synchronized OrientDBAdapter getInstance(OrientGraphNoTx graphNoTx) {
        if (instance == null) {
            instance = new OrientDBAdapter(graphNoTx);
        }
        return instance;
    }

    public OrientBaseGraph getGraph() {
        return graph;
    }

    public void setGraph(OrientBaseGraph graph) {
        this.graph = graph;
    }

    public static synchronized OrientDBAdapter getInstance(OrientGraph graphTx) {
        if (instance == null) {
            instance = new OrientDBAdapter(graphTx);
        }
        return instance;
    }

    public int executeQuery(String query) {
        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();
        int count = Iterables.size(vertices);
        return count;
    }
}
