package eu.fabiorinnone.faersbenchmark.queryloaders;

import java.util.ArrayList;

/**
 * Created by fabior on 09/03/16.
 */
public class MySQLQueryLoader extends AbstractQueryLoader {

    private static MySQLQueryLoader instance = null;

    private MySQLQueryLoader () {
        queries = new ArrayList<String>();
        index = 0;
    }

    public static synchronized MySQLQueryLoader getInstance() {
        if (instance == null) {
            instance = new MySQLQueryLoader();
        }
        return instance;
    }
}
