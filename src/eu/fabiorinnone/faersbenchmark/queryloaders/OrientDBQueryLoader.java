package eu.fabiorinnone.faersbenchmark.queryloaders;

import java.util.ArrayList;

/**
 * Created by fabior on 09/03/16.
 */
public class OrientDBQueryLoader extends AbstractQueryLoader {

    private static OrientDBQueryLoader instance = null;

    private OrientDBQueryLoader () {
        queries = new ArrayList<String>();
        index = 0;
    }

    public static synchronized OrientDBQueryLoader getInstance() {
        if (instance == null) {
            instance = new OrientDBQueryLoader();
        }
        return instance;
    }
}
