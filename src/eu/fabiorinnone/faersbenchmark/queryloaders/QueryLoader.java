package eu.fabiorinnone.faersbenchmark.queryloaders;

import java.io.IOException;

/**
 * Created by fabior on 09/03/16.
 */
public interface QueryLoader {

    public void setQueriesFile(String queriesFileName);

    public void loadQueries() throws IOException;

    public String nextQuery();

    public void reset();
}
