package eu.fabiorinnone.faersbenchmark.queryloaders;

import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/**
 * Created by fabior on 09/03/16.
 */
public abstract class AbstractQueryLoader implements QueryLoader {
    protected ArrayList<String> queries;
    protected int index;

    protected BufferedReader reader;
    protected String fileName;
    protected File file;

    protected Properties prop;

    protected String queriesPath;
    protected String queriesFileName;

    protected void setProperties(String fileName) throws FileNotFoundException,
            IOException,
            InvalidPropertiesFormatException {
        this.prop = new Properties();
        FileInputStream fis = new FileInputStream(fileName);
        prop.loadFromXML(fis);

        this.queriesPath = this.prop.getProperty("queries_path");
        this.queriesFileName = this.prop.getProperty("queries_file");
    }

    @Override
    public void setQueriesFile(String fileName) {
        this.queriesPath = FilenameUtils.getFullPath(fileName);
        this.queriesFileName = FilenameUtils.getName(fileName);
    }

    @Override
    public void loadQueries() throws IOException {
        file = new File(queriesPath + "/" + queriesFileName);
        reader = new BufferedReader(new FileReader(file));
        int lineNumber = 0;
        String line;
        while ((line = reader.readLine()) != null) {
            lineNumber++;
            if (!line.startsWith("#"))
                queries.add(line);
        }
    }

    @Override
    public String nextQuery() {
        if (index < queries.size()) {
            return queries.get(index++);
        }
        return null;
    }

    @Override
    public void reset() {
        index = 0;
    }
}
