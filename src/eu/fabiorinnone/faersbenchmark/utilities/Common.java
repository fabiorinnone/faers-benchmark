package eu.fabiorinnone.faersbenchmark.utilities;

/**
 * Created by fabior on 25/07/16.
 */
public class Common {

    public static int RETRIES = 1;

    public static int TIMEOUT = 60 * 60 * 1000;

    public static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static int EXIT_ERROR_CODE_1 = 1;
}
