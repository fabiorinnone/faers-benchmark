package eu.fabiorinnone.faersbenchmark.utilities;

import com.orientechnologies.orient.core.sql.OSQLEngine;
import com.orientechnologies.orient.core.sql.functions.OSQLFunction;
import com.orientechnologies.orient.graph.sql.functions.OGraphFunctionFactory;
import org.jetbrains.annotations.Contract;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Created by fabior on 09/03/16.
 */
public class Utilities {

    @Contract(pure = true)
    public static double getTimeElapsed(long timeStart, long timeEnd) {
        return ((double)(timeEnd-timeStart))/(1000.0);
    }

    public static void registerGraphFunctions() {
        OGraphFunctionFactory graphFunctions = new OGraphFunctionFactory();
        Set<String> names = graphFunctions.getFunctionNames();

        for (String name : names) {
            System.out.println("ODB graph function found: [" + name + "]");
            OSQLEngine.getInstance().registerFunction(name, graphFunctions.createFunction(name));
            OSQLFunction function = OSQLEngine.getInstance().getFunction(name);
            if (function != null) {
                System.out.println("ODB graph function [" + name + "] is registered: [" + function.getSyntax() + "]");
            }
            else {
                System.out.println("ODB graph function [" + name + "] NOT registered!");
            }
        }
    }

    public static String getDateTimeFormatted(DateTime dateTime) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(Common.DATE_TIME_PATTERN);
        String dateTimeFormatted = dateTime.toString(fmt);
        return dateTimeFormatted;
    }

    public static void printHeapMemoryInfo() {
        MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();
        MemoryUsage heapUsage = memoryBean.getHeapMemoryUsage();
        long maxMemory = heapUsage.getMax();
        long usedMemory = heapUsage.getUsed();
        System.out.print("Java current heap space is " + usedMemory + "/" + maxMemory);
        BigDecimal totalMemoryMb =
                new BigDecimal(usedMemory / Math.pow(1024, 2)).setScale(2, BigDecimal.ROUND_CEILING);
        BigDecimal maxMemoryMb =
                new BigDecimal(maxMemory / Math.pow(1024, 2)).setScale(2, BigDecimal.ROUND_CEILING);
        System.out.println(" (" + totalMemoryMb + " MB/" + maxMemoryMb + " MB)");
    }
}
