package eu.fabiorinnone.faersbenchmark.main;

import com.orientechnologies.orient.core.exception.OStorageException;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import eu.fabiorinnone.faersbenchmark.database.MySQLAdapter;
import eu.fabiorinnone.faersbenchmark.database.OrientDBAdapter;
import eu.fabiorinnone.faersbenchmark.queryloaders.MySQLQueryLoader;
import eu.fabiorinnone.faersbenchmark.queryloaders.OrientDBQueryLoader;
import eu.fabiorinnone.faersbenchmark.utilities.Common;
import eu.fabiorinnone.faersbenchmark.database.JDBCUtilities;
import eu.fabiorinnone.faersbenchmark.database.OrientDBUtilities;
import eu.fabiorinnone.faersbenchmark.utilities.Utilities;
import org.joda.time.DateTime;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Main {

    private static final String BUNDLE = "eu.fabiorinnone.faersbenchmark.main.resources.database";

    private static ResourceBundle bundle = null;

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: java -jar faers-benchmark.jar "
                    + "mysql-queries-file.txt orientdb-queries-file.txt");
            System.exit(1);
        }

        String mysqlQueriesFileName = args[0];
        String orientdbQueriesFileName = args[1];

        System.out.println("Initialization...");
        Utilities.registerGraphFunctions();
        System.out.println();

        System.out.println("Estabilishing connection to MySQL...");

        HashMap<String,String> mysqlProperties = new HashMap<String,String>();
        HashMap<String,String> orientdbProperties = new HashMap<String,String>();

        mysqlProperties.put("mysql.dbms", getString("mysql.dbms"));
        mysqlProperties.put("mysql.driver", getString("mysql.driver"));
        mysqlProperties.put("mysql.database_name", getString("mysql.database_name"));
        mysqlProperties.put("mysql.user_name", getString("mysql.user_name"));
        mysqlProperties.put("mysql.password", getString("mysql.password"));
        mysqlProperties.put("mysql.server_name", getString("mysql.server_name"));
        mysqlProperties.put("mysql.port_number", getString("mysql.port_number"));

        orientdbProperties.put("orientdb.dbms", getString("orientdb.dbms"));
        orientdbProperties.put("orientdb.database_name", getString("orientdb.database_name"));
        orientdbProperties.put("orientdb.user_name", getString("orientdb.user_name"));
        orientdbProperties.put("orientdb.password", getString("orientdb.password"));
        orientdbProperties.put("orientdb.database_path", getString("orientdb.database_path"));
        orientdbProperties.put("orientdb.engine", getString("orientdb.engine"));

        File resultsDir = new File("results/");
        if (!resultsDir.exists())
            resultsDir.mkdir();

        File resultsFile = new File("results/results.csv");

        if (!resultsFile.exists())
            try {
                resultsFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        else
            resultsFile.delete();

        JDBCUtilities jdbcUtilities = null;
        Connection mySQLConnection = null;
        try {
            jdbcUtilities = new JDBCUtilities(mysqlProperties);
        } catch (IOException e) {
            System.err.println("Problem reading properties file");
            e.printStackTrace();
        }

        MySQLQueryLoader mySQLQueryLoader = MySQLQueryLoader.getInstance();
        OrientDBQueryLoader orientDBQueryLoader = OrientDBQueryLoader.getInstance();

        mySQLQueryLoader.setQueriesFile(mysqlQueriesFileName);
        orientDBQueryLoader.setQueriesFile(orientdbQueriesFileName);

        try {
            mySQLQueryLoader.loadQueries();
            orientDBQueryLoader.loadQueries();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\nEstabilishing connection to OrientDB...");

        OrientDBUtilities orientDBUtilities = null;
        OrientGraph graph = null;
        try {
            orientDBUtilities = new OrientDBUtilities(orientdbProperties);
        } catch (IOException e) {
            System.err.println("Problem reading properties file");
            e.printStackTrace();
        }

        OrientDBAdapter orientDBAdapter = null;
        try {
            graph = orientDBUtilities.getTxGraph();
            orientDBAdapter = OrientDBAdapter.getInstance(graph);
        } catch (OStorageException e) {
            System.out.println("Error occourred: " + e.getMessage());
            System.out.println("Exiting...");
            System.exit(Common.EXIT_ERROR_CODE_1);
        }

        long timeStart, timeEnd;

        try {
            mySQLConnection = jdbcUtilities.getConnection();
            MySQLAdapter mySQLAdapter = MySQLAdapter.getInstance(mySQLConnection, jdbcUtilities.dbName,
                    jdbcUtilities.dbms);

            DateTime startTime = DateTime.now();
            System.out.println("Start time: " + Utilities.getDateTimeFormatted(startTime) + "\n");

            String mysqlQuery;
            String orientdbQuery;
            while ((mysqlQuery = mySQLQueryLoader.nextQuery()) != null) {
                timeStart = System.currentTimeMillis();
                System.out.println("Executing MySQL query:\n'" + mysqlQuery + "'");
                int count = -1;
                for (int i = 0; i < Common.RETRIES; i++)
                    count = mySQLAdapter.executeQuery(mysqlQuery);
                timeEnd = System.currentTimeMillis();
                BigDecimal timeElapsed1 = new BigDecimal(Utilities.getTimeElapsed(timeStart, timeEnd) / Common.RETRIES)
                        .setScale(2, BigDecimal.ROUND_CEILING);

                System.out.println("Fetched items: " + count);
                if (Common.RETRIES > 1)
                    System.out.println("Average elapsed time (sec.): " + timeElapsed1);
                else
                    System.out.println("Elapsed time (sec.): " + timeElapsed1);

                BigDecimal timeElapsed2 = new BigDecimal(-1);
                orientdbQuery = orientDBQueryLoader.nextQuery();
                String orientdbQueryWithTimeout = OrientDBUtilities.addTimeoutToQuery(orientdbQuery);
                if (orientdbQuery != null) {
                    timeStart = System.currentTimeMillis();
                    System.out.println("Executing OrientDB query:\n'" + orientdbQuery + "'");
                    count = -1;
                    try { //issue #5
                        for (int i = 0; i < Common.RETRIES; i++)
                            count = orientDBAdapter.executeQuery(orientdbQueryWithTimeout);
                        timeEnd = System.currentTimeMillis();
                        timeElapsed2 = new BigDecimal(Utilities.getTimeElapsed(timeStart, timeEnd) / Common.RETRIES)
                                .setScale(2, BigDecimal.ROUND_CEILING);
                    } catch (Exception e) { //issue #17 workaround
                        System.err.println("An error occourred: " + e.getMessage());
                        graph = orientDBUtilities.getTxGraph();
                        orientDBAdapter.setGraph(graph);
                        timeElapsed2 = new BigDecimal(-1);
                    }

                    System.out.println("Fetched items: " + count);

                    if (Common.RETRIES > 1)
                        System.out.println("Average elapsed time (sec.): " + timeElapsed2);
                    else
                        System.out.println("Elapsed time (sec.): " + timeElapsed2);
                }
                System.out.println();

                writeResultToFile(mysqlQuery, timeElapsed1, orientdbQuery, timeElapsed2);
            }
        } catch (SQLException e) {
            JDBCUtilities.printSQLException(e);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            JDBCUtilities.closeConnection(mySQLConnection);
        }

        DateTime endTime = DateTime.now();
        System.out.println("End time: " + Utilities.getDateTimeFormatted(endTime));

        System.out.println("\nExiting...");
        System.exit(0);
    }

    private static void writeResultToFile(String query1, BigDecimal time1, String query2,
                                          BigDecimal time2) throws IOException {

        File resultsFile = new File("results/results.csv");
        BufferedWriter writer = new BufferedWriter(new FileWriter(resultsFile, true));
        if (resultsFile.length() == 0) {
            writer.write("MySQL query,Time elapsed,OrientDB query,Time elapsed\n");
        }
        writer.write("\"" + query1 + "\";" + time1 + ",\"" + query2 + "\"," + time2 + "\n");
        writer.close();
    }

    public static ResourceBundle getResourceBundle() {
        if(bundle == null) {
            bundle = ResourceBundle.getBundle(BUNDLE);
        }
        return bundle;
    }

    public static String getString(String key) {
        String value = null;
        try {
            value = getResourceBundle().getString(key);
        }
        catch(MissingResourceException e) {
            System.out.println("java.util.MissingResourceException: Couldn't find value for: " + key);
        }
        if(value == null) {
            value = "Could not find resource: " + key;
        }
        return value;
    }
}
